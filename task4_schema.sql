CREATE TABLE IF NOT EXISTS `products` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `description` TEXT NOT NULL,
  `price` decimal(10,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

INSERT INTO `products`
	(`name`, `description`, `price`)
	VALUES
	('item1', 'milk', 10.22),
  ('item2', 'bread', 11.65),
  ('item3', 'meat', 65.45),
  ('item4', 'potato', 3.45)
;

CREATE TABLE IF NOT EXISTS `clients` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

INSERT INTO clients
	(`name`, `email`, `phone`)
	VALUES
	('Ivan', 'ivan@gmail.com', '0681234567'),
  ('Kate', 'kate@gmail.com', '0685647898'),
  ('Serg', 'serg@gmail.com', '0676549832'),
  ('Gerg', 'gerg@gmail.com', '0681835487')
;

CREATE TABLE IF NOT EXISTS `orders` (
  `id` INT( 11 ) NOT NULL AUTO_INCREMENT,
  `client_id` INT( 11) NOT NULL,
  `amount` DECIMAL(12, 2) NOT NULL,
  `created` datetime NOT NULL,
  `ip` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_1_idx` (`client_id` ASC),
  CONSTRAINT `fk_order_1`
  FOREIGN KEY (`client_id`)
  REFERENCES `clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE  TABLE IF NOT EXISTS `order_product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_order_product_1_idx` (`order_id` ASC),
  INDEX `fk_order_product_2_idx` (`product_id` ASC),
  CONSTRAINT `fk_order_product_1`
  FOREIGN KEY (`order_id` )
  REFERENCES `orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_product_2`
  FOREIGN KEY (`product_id`)
  REFERENCES `products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
