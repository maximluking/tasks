SELECT `o`.`id`, `o`.`client_id`, `o`.`amount`, `o`.`created`, `o`.`ip`
FROM `orders` `o` INNER JOIN `order_product` `op`
ON `o`.`id` = `op`.`order_id`
WHERE `o`.`client_id` = 1;