SELECT `o`.`id`, `o`.`created`,
  SUM(`op`.`quantity`),
  AVG(`p`.`price`)
FROM `orders` `o` INNER JOIN `order_product` `op`
  ON `o`.`id` = `op`.`order_id`
  INNER JOIN `products` `p`
  ON `op`.`product_id` = `p`.`id`
  GROUP BY `o`.`id`
  HAVING SUM(`op`.`quantity`) > 1
ORDER BY `o`.`created` ASC;