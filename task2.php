<?php

function checkBrackets($s)
{
    $symbolArr = [
        '(' => ')',
        '[' => ']',
        '{' => '}'
    ];

    $flipSymbolArr = array_flip($symbolArr);
    $symbolKeys = array_keys($symbolArr);
    $symbolValues = array_values($symbolArr);
    $arr = str_split($s);
    $stackArr = [];

    foreach ($arr as $value) {
        if (in_array($value, $symbolKeys)) {
            array_push($stackArr, $value);
        } elseif (in_array($value, $symbolValues)) {
            if (!empty($stackArr) && $stackArr[count($stackArr) - 1] === $flipSymbolArr[$value]) {
                array_pop($stackArr);
            } elseif (empty($stackArr) || !empty($stackArr)&&$stackArr[count($stackArr) - 1] !== $flipSymbolArr[$value]) {
                return false;
            }
        }
    }

    if (empty($stackArr)) {
        return true;
    } else {
        return false;
    }
}
