<?php

function myStrRev($s)
{
    $arr = preg_split('//u', $s, -1, PREG_SPLIT_NO_EMPTY);
    $arrRev = [];

    for ($i = count($arr); $i >= 0; $i--) {
        $arrRev[] = $arr[$i];
    }

    return implode($arrRev);
}
