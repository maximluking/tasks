START TRANSACTION;
INSERT INTO `orders`
	(`client_id`, `amount`, `created`, `ip`)
VALUES
	(1, 219, NOW(), '127.0.0.1')
;
SELECT
  @id:=MAX(id)
FROM
  `orders`;
INSERT INTO `order_product`
	(`order_id`, `product_id`, `quantity`)
VALUES
	(@id, 1, 20),
  (@id, 2, 35),
  (@id, 3, 23),
  (@id, 4, 15)
;
INSERT INTO `orders`
	(`client_id`, `amount`, `created`, `ip`)
VALUES
	(2, 215, NOW(), '127.1.1.1')
;
SELECT
  @id:=MAX(id)
FROM
  `orders`;
INSERT INTO `order_product`
	(`order_id`, `product_id`, `quantity`)
VALUES
	(@id, 1, 20),
  (@id, 2, 35),
  (@id, 3, 23),
  (@id, 4, 14)
;
INSERT INTO `orders`
	(`client_id`, `amount`, `created`, `ip`)
VALUES
	(3, 230, NOW(), '127.1.1.1')
;
SELECT
  @id:=MAX(id)
FROM
  `orders`;
INSERT INTO `order_product`
	(`order_id`, `product_id`, `quantity`)
VALUES
	(@id, 1, 35),
  (@id, 2, 35),
  (@id, 3, 23),
  (@id, 4, 14)
;
INSERT INTO `orders`
	(`client_id`, `amount`, `created`, `ip`)
VALUES
	(3, 350, NOW(), '127.1.1.1')
;
SELECT
  @id:=MAX(id)
FROM
  `orders`;
INSERT INTO `order_product`
	(`order_id`, `product_id`, `quantity`)
VALUES
	(@id, 3, 50),
  (@id, 4, 50)
;
INSERT INTO `orders`
	(`client_id`, `amount`, `created`, `ip`)
VALUES
	(3, 303.12, NOW(), '127.1.1.1')
;
SELECT
  @id:=MAX(id)
FROM
  `orders`;
INSERT INTO `order_product`
	(`order_id`, `product_id`, `quantity`)
VALUES
	(@id, 1, 50.56),
  (@id, 3, 50.48)
;
COMMIT;
