SELECT `o`.`id`, `o`.`client_id`, `o`.`amount`, `o`.`created`, `o`.`ip`
FROM `orders` `o` INNER JOIN `order_product` `op`
ON `o`.`id` = `op`.`order_id`
WHERE `op`.`product_id` = 2;