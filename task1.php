<?php

function searchInArray($arr, $value)
{
    $left = 0;
    $right = count($arr) - 1;

    while ($left <= $right) {
        $middle = round(($right + $left)/2, 0);
        if ($value === $arr[$middle]) {
            return true;
        } elseif ($value > $arr[$middle]) {
            $left = $middle + 1;
        } else {
            $right = $middle - 1;
        }
    }
	return false;
}
